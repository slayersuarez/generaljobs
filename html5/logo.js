(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 290,
	height: 125,
	fps: 30,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.logo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_249 = function() {
		this.gotoAndPlay(60)
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(249).call(this.frame_249).wait(1));

	// mascara (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_54 = new cjs.Graphics().p("AvWIaQgFgEABgMIAAqdQAAgNADgEQAEgFAMABQCGABCFgBQAMAAADADQAEADAAAMIgBHRQAAAmgMApIAAACQgLAogXAcQgMAOgQAMQgnAdhAAOQgpAIhDAAIgCABQgKAAgDgEgAzjFQQhYhCgUhoQgbiFBShjQBShlCHgBQAegBAFAFQAFAFAAAeIAAC9QAAALgEAEQgEADgLAAIidgBQgJAAgDADQgEADAAAKQABA7gBA7QAAAKADADQAEADAJAAQAbAAAEgEQAEgEAAgaIAAgxQAAgHACgCQADgDAGAAQA7ABA7gBQAIAAACAEQACADAAAIIAADnQAAAIgCACQgCAEgHAAIgbACQhZAAhNg6gAKXFBQgTgLgEgPIgHgNQgDgHAJgEQAMgFAFAAQAKAAAEAOQAIAZAfgCQAdAAALgdQAFgPAAgcIAAiqQAAgKADgDQAEgEAJAAQAJAAAEAEQADADAAAJIgBBiIAABgQgBAwgoAWQgTAKgTAAQgVAAgWgNgAnxFMQgxgEgogkQgegcgJgwQgKgvAQgpQARgpAjgYQAjgYAsgBQA8gCAiAbIALAHQAFAFgHAHQgHAJgEACQgIAFgKgIQgpghgyALQgzAKgXAtQgTAkAEAnQADAnAYAcQAYAbAkAJQAjAIAogNQAQgFAGgIQAHgIgDgPIgBgIIACgdQAAgRgFgIQgFgIgRACQgTACgJAAQgIgBgCgDQgDgDAAgHQAAgJACgDQADgEAJAAQAjACAjgCQAJAAADAEQACADAAAJIAAA5IAAAUQgBANABAIQACAPgGAKQgGAIgPAFQguARglAAIgOgBgABGE5QgcgSgJgkQgKgjANggQAOghAdgPQAdgPAjAHQAhAGAUAbQAUAagCAiQgBAHgEADQgCABgHAAIg9AAIg9AAQgNgBgCAGQgCAFAEALQANAiAjAHQAjAHAbgaIAHgIQAEgDAHAFQAJAHABAFQABAGgJAIQgZAXgkADIgKABQgeAAgYgRgAB4CgQgTABgSAPQgRAOgEAQQgBAGADADQACABAFAAIBeAAQALABADgFQADgEgDgKQgGgTgQgKQgOgJgSAAIgFAAgAk6E4QgcgTgIgkQgJgkAOggQAPggAegOQAfgPAjAJQAeAHATAcQATAcgDAeQgBAHgEACQgCABgGAAIh6AAQgNAAgDAFQgDAEAGANQAOAhAiAHQAjAHAZgaQAIgIAGAAQAEABAIAHQAIAHgEAGIgKAIQgYAXglACIgIABQgfAAgZgSgAkGCgQgTABgSAOQgRANgEARQgCAHADACQACACAGAAIBeAAQALABADgFQACgFgDgKQgGgSgQgKQgOgJgSAAIgEAAgAF4FJQgWgBgPgNQgPgMgDgSQgEgWAKgRQAKgSAWgIQAdgMAqADQAMABADgFQAEgFgEgLQgFgSgPgIQgQgIgVAEQgHAAgGAEQgRAJgGABQgMACgOgLQgSgOgYATQgXATgBAbQgCAfABA9QABAJgDAEQgEADgJAAQgJABgDgFQgCgDAAgJIgBiWQgBgJAAgEQABgHALgBQAMgBAEAGQADAGgCAPQAgglApAIQAGACgBAGQgBAHACACQAsgZAuAJQAWAEANAPQANAPABAVIAAA+QAAAmADAYQACALgKgBIgLAAQgHAAgCgHIgDgPQgPAQgSAGQgNAFgSAAIgHgBgAGkDqQgkACgRAKQgRAKABARQABASATAJQAWAJAWgJQAVgKAIgXQAHgVgEgGQgEgGgUAAIgDAAgANDEuQgcgaAAgrQgBgrAcgcQAcgcArAAQAsgBAcAcQAcAbAAArQAAAsgbAbQgcAbgsAAQgsAAgbgbgANZC1QgUAVABAfQAAAdAUAVQATAVAdAAQAcABAUgVQAUgWAAgfQAAgegUgVQgTgUgdAAQgdAAgUAVgAQfEwQgCABAAALQAAAJgKAAQgKAAgEgCQgFgEAAgKIABkLQAAgJACgDQADgEAJAAQAKAAADAFQABADAAAJIAAB4QAmgdAnAAQAhAAAYAUIAKAJQAZAZABAmQACAngVAcQgIAKgJAHQgUARgbACIgGAAQgkAAgrgZgAQxC1QgVAUAAAfQAAAdAWAWQAVAWAdgBQAdAAAUgVQAJgKAFgMQAFgNABgPQAAgRgGgNQgFgMgJgKQgTgVgeAAQgeAAgVAVgATOExQgFgFgCgDQgDgFAHgEQAEgCAIgHQAHgEAHAJQAUAaAlgJQAWgFADgTQACgTgYgIIgtgMQgpgMABglQAAgSAKgOQALgOATgFQA1gOAeAeIAHAIQADAGgHAFQgIAHgEACQgHADgIgKQgKgMgOgDQgNgDgOAHQgQAIAAAPQAAAOAPAGQAIAEAeAHQA6AOgFAvQgCAXgSANQgSANgcABIgGAAIgDAAQgoAAgVgYgAICFBQgDgDAAgKQABiFgBiGQAAgJADgDQAEgEAJAAQAJAAADAEQADADgBAJIAAELQAAAKgBADQgDAEgKAAIgCABQgIAAgDgFgAiXE+IAAgOIAAhBIgCAAIAAhWIAAgJQABgFAFAAIAOgCQAHAAACAJQACAIAEABQADAAAGgFQARgNAWgDQAhgEAVAQQAUAQACAgQACAYgBAkIAAA8QAAAIgFACIgJABQgOABAAgOQABhHgBgjQgCglgggIQgPgDgPAGQgPAGgKANQgMARAAAcIAABMIgBAOQgCAHgLAAIgCABQgLAAgCgIgAxAkYQgogogEg2IAAgOQAAg/AsgtQATgTAXgLQAegOAkgBQAkAAAfAPQAWALATATQAtAtAABAIAAANQgFA2goAoQgtAtg+AAQhAAAgtgtg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(54).to({graphics:mask_graphics_54,x:144.2,y:62.4}).wait(196));

	// brillo
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(255,255,255,0)","#FFFFFF","#FFFFFF","rgba(255,255,255,0)"],[0,0.443,0.494,1],-23.1,-11,23.1,11).s().p("ApFJzIK83BIHPDcIq8XBg");
	this.shape.setTransform(-67.8,49.4);

	this.instance = new lib.Interpolación1("synched",0);
	this.instance.setTransform(-67.8,49.4);
	this.instance._off = true;

	this.instance_1 = new lib.Interpolación2("synched",0);
	this.instance_1.setTransform(360.2,49.4);

	this.shape.mask = this.instance.mask = this.instance_1.mask = mask;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},54).to({state:[{t:this.instance}]},55).to({state:[{t:this.instance_1}]},36).wait(105));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(109).to({_off:false},0).to({_off:true,x:360.2},36).wait(105));

	// jobs
	this.instance_2 = new lib.jobs();
	this.instance_2.setTransform(245,80.6,0.153,0.153);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(44).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},10).wait(196));

	// general
	this.instance_3 = new lib.general();
	this.instance_3.setTransform(140.6,80.5,0.117,0.117);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(34).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},10).wait(206));

	// circulo
	this.instance_4 = new lib.circulo();
	this.instance_4.setTransform(46.9,23.6,0.222,0.222);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(19).to({_off:false},0).to({scaleX:1.19,scaleY:1.19,alpha:1},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},5,cjs.Ease.get(1)).wait(216));

	// j
	this.instance_5 = new lib.J();
	this.instance_5.setTransform(61.5,-42.9);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(9).to({_off:false},0).to({y:81.1},10).wait(231));

	// g
	this.instance_6 = new lib.G();
	this.instance_6.setTransform(24.3,161.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({y:73.8},9,cjs.Ease.get(1)).wait(241));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(153.3,196.1,31.9,56.4);


// symbols:
(lib.jobs = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9A9A9A").s().p("Ag/BJQgFgFgCgDQgDgFAHgEQAEgCAIgHQAHgEAHAJQAUAaAjgJQAWgFADgTQACgTgYgIIgrgMQgpgKABglQAAgSAKgOQALgOATgFQAzgOAeAeIAHAIQADAGgHAFQgIAHgEACQgHADgIgKQgKgMgOgDQgLgDgOAHQgQAIAAAPQAAAOAPAGQAIAEAcAHQA6AMgFAvQgCAXgSANQgSANgcABIgFAAIgCAAQgpAAgUgYg");
	this.shape.setTransform(29.5,5.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9A9A9A").s().p("AhGBGQgbgagBgrQAAgpAcgcQAbgcArAAQAqgBAcAcQAcAbABAqQAAArgcAbQgbAbgsAAQgqAAgcgbgAgvgxQgUAVAAAdQAAAdAUAVQAUAVAbAAQAcABAUgVQATgWAAgeQAAgdgTgVQgUgUgcAAQgcAAgTAVg");
	this.shape_1.setTransform(-9.3,5.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#9A9A9A").s().p("AhEB+QgCABAAALQAAAJgKAAQgKAAgDgCQgFgEAAgKIAAkJQAAgJADgDQADgEAJAAQAJAAADAFQACADAAAJIgBB4QAngdAlAAQAnAAAcAdQAYAXACAmQACAngWAcQgZAhgmADIgHAAQgiAAgrgZgAgxADQgVAUAAAfQAAAdAVAWQAWAWAbgBQAcAAAUgVQAUgVAAgdQABgfgUgVQgUgTgdAAQgdAAgUATg");
	this.shape_2.setTransform(12.5,-0.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9A9A9A").s().p("AgtCEQgSgMgFgOIgGgNQgDgIAJgEQALgFAFAAQAKABAEANQAIAZAegBQAcgBALgdQAGgPAAgbIAAipQgBgJAEgEQADgEAKAAQAJABADADQADAEAAAJIAABiIAABeQgCAwgoAVQgTALgRAAQgVAAgWgNg");
	this.shape_3.setTransform(-29,0.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#9A9A9A").s().p("Ag/BJQgFgFgCgDQgDgFAHgEQAEgCAIgHQAHgEAHAJQAUAaAjgJQAWgFADgTQACgTgYgIIgrgMQgpgKABglQAAgSAKgOQALgOATgFQAzgOAeAeIAHAIQADAGgHAFQgIAHgEACQgHADgIgKQgKgMgOgDQgLgDgOAHQgQAIAAAPQAAAOAPAGQAIAEAcAHQA6AMgFAvQgCAXgSANQgSANgcABIgFAAIgCAAQgpAAgUgYg");
	this.shape_4.setTransform(29.5,5.2);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-36.7,-15.4,73.5,30.9);


(lib.J = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#07729A").s().p("AiTFgQgFgEABgMIAAqdQAAgNADgEQAEgFAMABQCEABCFgBQAMAAADADQAEADAAAMIgBHRQAAAngMAqQgQA8guAiQgnAdg+AOQgpAIhDAAIgCAAQgKAAgDgDg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-15.2,-35.5,30.6,71.2);


(lib.Interpolación2 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(255,255,255,0)","#FFFFFF","#FFFFFF","rgba(255,255,255,0)"],[0,0.443,0.494,1],-23.1,-11,23.1,11).s().p("ApFJzIK83BIHPDcIq8XBg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-58.2,-84.7,116.5,169.5);


(lib.Interpolación1 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(255,255,255,0)","#FFFFFF","#FFFFFF","rgba(255,255,255,0)"],[0,0.443,0.494,1],-23.1,-11,23.1,11).s().p("ApFJzIK83BIHPDcIq8XBg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-58.2,-84.7,116.5,169.5);


(lib.general = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9A9A9A").s().p("AhNBYIgBgOIAAhBIgBAAIAAhUIgBgJQABgFAGAAIANgCQAIAAACAJQABAIAFABQACAAAGgFQASgNAUgDQAhgEAVAQQAVAQACAgQACAYAAAiIAAA8QAAAIgFACIgLABQgOABAAgOQABhHgCghQgCglgggIQgPgDgMAGQgPAGgKANQgNARAAAcIAABKIAAAOQgCAHgMAAIgBABQgLAAgCgIg");
	this.shape.setTransform(-2.9,5.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9A9A9A").s().p("AgyBRQgcgSgJgkQgKghANggQAOghAdgPQAdgPAhAHQAhAGAUAbQAUAagCAiQgBAFgEADQgCABgHAAIg9AAIg7AAQgNgBgCAGQgCAFAEALQANAiAjAHQAhAHAbgaIAHgIQAEgDAHAFQAJAHABAFQABAGgJAIQgZAXgkADIgKAAQgcAAgYgQgAAAhGQgTABgSAPQgRAOgEAQQgBAGADADQACABAFAAIBcAAQALABADgFQADgEgDgKQgGgTgQgKQgOgKgSAAIgDABg");
	this.shape_1.setTransform(16.6,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#9A9A9A").s().p("Ag0BQQgbgTgJgkQgJgiAPggQAOggAfgOQAegPAhAJQAeAHATAcQAUAcgEAeQAAAFgEACQgCABgHAAIh4AAQgNAAgCAFQgDAEAFANQAPAhAiAHQAgAHAZgaQAIgIAGAAQAEABAJAHQAIAHgFAGIgKAIQgYAXgkACIgIAAQgeAAgZgRgAAAhGQgTABgRAOQgSANgEARQgBAHACACQACACAHAAIBcAAQALABADgFQACgFgEgKQgGgSgPgKQgOgJgSAAIgDAAg");
	this.shape_2.setTransform(-21.8,5.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9A9A9A").s().p("AAWBhQgWgBgNgNQgPgMgDgSQgEgWAKgRQAKgQAUgIQAdgMAqADQAMABADgFQAEgFgEgLQgFgSgPgIQgQgIgVAEQgHAAgGAEQgRAJgEABQgMACgOgLQgSgOgYATQgXATgBAbQgCAdABA9QABAJgDAEQgEADgJAAQgJABgDgFQgCgDAAgJIgBiUQgBgJAAgEQABgHALgBQAMgBAEAGQADAGgCAPQAgglApAIQAGACgBAGQgBAHACACQAqgZAuAJQAWAEANAPQANAPABAVIAAA8QAAAmADAYQACALgKgBIgLAAQgHAAgCgHIgDgPQgPAQgSAGQgNAEgSAAIgHAAgABCACQgkACgRAKQgPAKABARQABASARAJQAWAJAWgJQAVgKAIgXQAHgVgEgGQgEgGgUAAIgDAAg");
	this.shape_3.setTransform(39.8,5.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#9A9A9A").s().p("AgLCSQgDgEAAgKQABiEgBiEQAAgKADgDQAEgDAHAAQAJgBADAEQADADgBAKIAAEJQAAAJgBADQgDAFgKgBIAAABQgIAAgDgEg");
	this.shape_4.setTransform(57.1,-0.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#9A9A9A").s().p("AhNBYIgBgOIAAhBIgBAAIAAhUIgBgJQABgFAGAAIANgCQAIAAACAJQABAIAFABQACAAAGgFQASgNAUgDQAhgEAVAQQAVAQACAgQACAYAAAiIAAA8QAAAIgFACIgLABQgOABAAgOQABhHgCghQgCglgggIQgPgDgMAGQgPAGgKANQgNARAAAcIAABKIAAAOQgCAHgMAAIgBABQgLAAgCgIg");
	this.shape_5.setTransform(-2.9,5.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#9A9A9A").s().p("AAECSQgvgEgogkQgegcgJgvQgKguAQgpQARgpAjgYQAjgYAqgBQA8gBAiAbIALAGQAFAFgHAHQgHAJgEADQgIAFgKgJQgpgggwAKQgzALgXAtQgTAjAEAlQADAoAYAbQAYAcAkAIQAhAIAogNQAQgFAGgHQAHgJgDgPIgBgIIACgcQAAgRgFgIQgFgIgRABQgTADgJgBQgIAAgCgDQgDgDAAgGQAAgIACgDQADgFAJABQAjABAjgBQAJgBADAEQACAEAAAIIAAA3IAAAVQgBAMABAIQACAQgGAJQgGAJgPAFQguAQglAAIgOgBg");
	this.shape_6.setTransform(-45.9,0.6);

	this.addChild(this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-58.6,-15.3,117.2,30.7);


(lib.G = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#07729A").s().p("AgrDgQhYhCgVhoQgaiDBShlQBShlCEgBQAegBAFAFQAFAFAAAeIABC/QAAALgEAEQgEADgLAAIibgBQgKAAgDADQgEADAAAKQACA5gCA7QAAAKAEADQADADAKAAQAYAAAEgEQAFgEAAgaIAAgxQgBgHADgCQACgDAHAAQA7ABA6gBQAIAAADAEQABADAAAIIAADnQAAAIgBACQgCAEgIAAIgbABQhZAAhKg5g");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-15.9,-28.2,31.9,56.4);


(lib.circulo = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9A9A9A").s().p("AhrBrQgtgsABg/QAAg+AsgsQAtgtA+AAQA+AAAtAtQAtAtAAA9QAAA/gtAtQgtAsg+AAQg+AAgtgtg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-15.2,-15.2,30.6,30.6);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;