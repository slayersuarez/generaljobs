<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >

	<head>
		<jdoc:include type="head" />
		<meta charset="utf-8"/>

		<!-- Estos son los estilos -->
		<link rel="stylesheet" type="text/css" href="less/load-styles.php?load=home">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/reset.css">

		<!--Estas son las fuentes-->
		<link href='http://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Comfortaa:400,300,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Archivo+Narrow:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
		<!--SCRIPTS-->
		<script type="text/javascript">
	    (function( $ , window , document ){

	    function inicio( body ){
	        var ancho= body.innerWidth;
	                if( ancho >= 580){
	                  var margen = '-'+((1900-ancho)/2);
	                }else{  var margen = 0; }
	                var x;
	                x = $( ".banner-home" );
	                x.css("margin-left",margen+"px");
	            }


	            $( window ).resize( function(){ inicio( this ); } );
	            $( document ).ready(function(){ 
	            inicio( this ); 
	            $( window ).trigger( 'resize' );
	            });

	        })( jQuery, this,  this.document , 'undefined');
	    </script>

	</head>

	<body>

		<header>
			<div class="content-header">
				<div class="logo">
					<iframe src="html5/logo.html" width="295" height="130"></iframe>
				</div>
				<div class="derecha">
					<jdoc:include type="modules" name="idiomas" style="xhtml"/>
					<jdoc:include type="modules" name="registrate" style="xhtml"/>
				</div>
			</div>
		</header>
		<nav>
			<div class="contenedor-menu">
				<jdoc:include type="modules" name="menu-principal" style="xhtml"/>
			</div>
		</nav>

		<div class="banner">
			<div class="banner-home">
				<jdoc:include type="modules" name="banner" style="xhtml"/>
			</div>
			<div class="login-pagina">
				<jdoc:include type="modules" name="login" style="xhtml"/>
			</div>
		</div>

		<main>
			<div class="wrapper-1">
				<div class="content-all">
					<jdoc:include type="modules" name="bienvenidos" style="xhtml"/>
				</div>
			</div>
			<div class="wrapper-2">
				<div class="content-all">
					<jdoc:include type="modules" name="imagenes" style="xhtml"/>
				</div>
			</div>
		</main>	

		<footer>
			<div class="wrapper-pie">
				<jdoc:include type="modules" name="pie" style="xhtml"/>
			</div>
			<div class="creditos">
				<div class="copy">
					<span class="sainet">
						<a target="_blank" href="http://www.creandopaginasweb.com">
							Página web diseñada por <img alt="Diseño de paginas web" src="http://www.creandopaginasweb.com/theme/img/logoverde.png">
						</a>
					</span>
				</div>
			</div>
		</footer>
	</body>
</html>
